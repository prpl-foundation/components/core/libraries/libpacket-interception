/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>

#include "packet-interception/variant_packet_verdict.h"

static int variant_packet_verdict_init(amxc_var_t* const var) {
    int retval = -1;
    packet_verdict_t* var_packet_verdict = NULL;

    var_packet_verdict = (packet_verdict_t*) calloc(1, sizeof(packet_verdict_t));
    when_null(var_packet_verdict, exit);

    var->data.data = var_packet_verdict;
    retval = 0;

exit:
    return retval;
}

static void variant_packet_verdict_delete(amxc_var_t* var) {
    packet_verdict_t* var_packet_verdict = (packet_verdict_t*) var->data.data;
    var->data.data = NULL;
    free(var_packet_verdict);
}

static int variant_packet_verdict_copy(amxc_var_t* const dest,
                                       const amxc_var_t* const src) {
    const packet_verdict_t* src_var_packet_verdict = (packet_verdict_t*) src->data.data;
    packet_verdict_t* dest_var_packet_verdict = (packet_verdict_t*) dest->data.data;

    memcpy(dest_var_packet_verdict, src_var_packet_verdict, sizeof(packet_verdict_t));

    return 0;
}

static int variant_packet_verdict_move(amxc_var_t* const dest,
                                       amxc_var_t* const src) {
    variant_packet_verdict_delete(dest);
    dest->data = src->data;
    src->data.data = NULL;
    return 0;
}

static amxc_var_type_t variant_packet_verdict = {
    .init = variant_packet_verdict_init,
    .del = variant_packet_verdict_delete,
    .copy = variant_packet_verdict_copy,
    .move = variant_packet_verdict_move,
    .convert_from = NULL,
    .convert_to = NULL,
    .compare = NULL,
    .get_key = NULL,
    .set_key = NULL,
    .get_index = NULL,
    .set_index = NULL,
    .type_id = 0,
    .hit = { .ait = NULL, .key = NULL, .next = NULL },
    .name = AMXC_VAR_NAME_PACKET_VERDICT
};

CONSTRUCTOR_LVL(200) static void variant_packet_verdict_register(void) {
    amxc_var_register_type(&variant_packet_verdict);
}

DESTRUCTOR_LVL(200) static void variant_packet_verdict_unregister(void) {
    amxc_var_unregister_type(&variant_packet_verdict);
}

const packet_verdict_t* amxc_var_get_const_packet_verdict_t(const amxc_var_t* const var) {
    const packet_verdict_t* retval = NULL;

    when_null(var, exit);
    when_true(var->type_id != AMXC_VAR_ID_PACKET_VERDICT, exit);

    retval = (packet_verdict_t*) var->data.data;
exit:
    return retval;
}

int amxc_var_set_packet_verdict_t(amxc_var_t* const var, const packet_verdict_t* const val) {
    packet_verdict_t* var_packet_verdict = NULL;
    int retval = -1;

    when_null(var, exit);
    when_null(val, exit);
    when_failed(amxc_var_set_type(var, AMXC_VAR_ID_PACKET_VERDICT), exit);

    var_packet_verdict = (packet_verdict_t*) var->data.data;
    when_null(var_packet_verdict, exit);

    memcpy(var_packet_verdict, val, sizeof(packet_verdict_t));

    retval = 0;

exit:
    return retval;
}
