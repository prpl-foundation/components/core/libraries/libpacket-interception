# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.4 - 2024-03-08(15:02:55 +0000)

### Fixes

- [PacketInterception] Add NFLOG support

## Release v1.0.3 - 2024-03-07(08:28:08 +0000)

### Other

- [ci] Disable debian package generation

## Release v1.0.2 - 2024-02-19(15:03:20 +0000)

### Changes

- [PacketInterception] Add NFLOG support

## Release v1.0.1 - 2023-07-10(09:07:08 +0000)

### Other

- [libpacket-Inspector] Wrong proprietary SoftAtHome copyright headers in source files

## Release v1.0.0 - 2023-03-21(09:01:44 +0000)

### Breaking

- [AMX][Packet-interception] Rework Communication Config

## Release v0.0.3 - 2022-11-29(14:35:34 +0000)

### Other

- Opensource component

## Release v0.0.2 - 2022-11-29(10:26:58 +0000)

### Fixes

- Add missing include of time.h

## Release v0.0.1 - 2022-11-17(09:31:40 +0000)

### Other

- [Packet Interception] Create the new Packet Interception component

